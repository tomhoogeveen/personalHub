<?php

namespace AppBundle\Controller;

use AppBundle\Service\GoogleCalendarHelper;
use AppBundle\Service\GreeterHelper;
use AppBundle\Service\PushBulletHelper;
use AppBundle\Service\WunderGroundHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        /** @var GoogleCalendarHelper $calendarHelper */
        $calendarHelper = $this->get("google.calenderHelper");
        $calendarEvents = $calendarHelper->getEvents(5);

        if($this->getParameter('settings.plexpyapikey') && $this->getParameter('settings.plexpylocation')) {
            $plexPyHelper = $this->get("plexpy.apiHelper");
            $recentlyAdded = $plexPyHelper->getRecentlyAdded(10);
        } else{
            $recentlyAdded = false;
        }

        if($this->getParameter('settings.channel_name')) {
            /** @var PushBulletHelper $pushBulletHelper */
            $pushBulletHelper = $this->get("pushbullet.apiHelper");
            $channelInfo = $pushBulletHelper->getChannelInfo($this->getParameter('settings.channel_name'));
        } else{
            $channelInfo = false;
        }

        if($this->getParameter('settings.wundeground_api_key')) {
            /** @var WunderGroundHelper $wundergroundHelper */
            $wundergroundHelper = $this->get("wunderground.apiHelper");
            $forecast = $wundergroundHelper->getWeather($this->getParameter('settings.wundeground_api_key'), $this->getParameter('settings.latitude'), $this->getParameter('settings.longitude'));
//            $forecast = false;
        } else{
            $forecast = false;
        }

        /** @var GreeterHelper $greeterHelper */
        $greeterHelper = $this->get("greeterHelper");
        $greet = $greeterHelper->getGreeting(8);

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'forecasts' => $forecast,
            'channelInfo' => $channelInfo,
            'calendarEvents' => $calendarEvents,
            'recentlyAdded' => $recentlyAdded,
            'greet' => $greet
        ]);
    }


    /**
     * @Route("/magicmirror", name="magicMirror")
     */
    public function MagicMirror(Request $request) {

        if($this->getParameter('settings.wundeground_api_key')) {
            /** @var WunderGroundHelper $wundergroundHelper */
            $wundergroundHelper = $this->get("wunderground.apiHelper");
            $forecast = $wundergroundHelper->getWeather($this->getParameter('settings.wundeground_api_key'), $this->getParameter('settings.latitude'), $this->getParameter('settings.longitude'));
//            $forecast = false;
        } else{
            $forecast = false;
        }

        /** @var GreeterHelper $greeterHelper */
        $greeterHelper = $this->get("greeterHelper");
        $greet = $greeterHelper->getGreeting();

        /** @var GoogleCalendarHelper $calendarHelper */
        $calendarHelper = $this->get("google.calenderHelper");
        $calendarEvents = $calendarHelper->getEvents(4);

        return $this->render('default/magicmirror.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'forecasts' => $forecast,
//            'channelInfo' => $channelInfo,
            'calendarEvents' => $calendarEvents,
//            'recentlyAdded' => $recentlyAdded,
            'greet' => $greet
        ]);

    }
    /**
     * @Route("/settings", name="settings")
     */
    public function settings(Request $request) {
        $errors = array();
        $form = $this->createFormBuilder()
            ->add('channel_name', TextType::class, array('mapped' => false, 'data' => $this->getParameter('settings.channel_name')))
            ->add('wundeground_api_key', TextType::class, array('mapped' => false, 'data' => $this->getParameter('settings.wundeground_api_key')))
            ->add('longitude', TextType::class, array('mapped' => false, 'data' => $this->getParameter('settings.longitude')))
            ->add('latitude', TextType::class, array('mapped' => false, 'data' => $this->getParameter('settings.latitude')))
            ->add('plexpy_api_key', TextType::class, array('mapped' => false, 'data' => $this->getParameter('settings.latitude')))
            ->add('plexpy_location', TextType::class, array('mapped' => false, 'data' => $this->getParameter('settings.latitude')))
            ->add('save', SubmitType::class, array('label' => 'Doorgaan', 'attr' => array('class' => 'button')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $channelName = $request->request->get("channel_name");
            $wundergroundApiKey = $request->request->get("wundeground_api_key");
            $longitude = $request->request->get("longitude");
            $latitude = $request->request->get("latitude");
            $plexpy_api_key = $request->request->get("plexpy_api_key");
            $plexpy_location = $request->request->get("plexpy_location");


            $this->setParameter('settings.channel_name', $channelName);
            $this->setParameter('settings.wundeground_api_key', $wundergroundApiKey);
            $this->setParameter('settings.longitude', $longitude);
            $this->setParameter('settings.latitude', $latitude);
            $this->setParameter('settings.plexpyapikey', $plexpy_api_key);
            $this->setParameter('settings.plexpylocation', $plexpy_location);

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Settings updated');

        }
        return $this->render('settings/settings.html.twig', array(
            'form' => $form->createView(),
            'errors' => $errors
        ));

    }
}
