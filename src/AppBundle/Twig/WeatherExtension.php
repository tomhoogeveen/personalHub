<?php
// src/AppBundle/Twig/AppExtension.php
namespace AppBundle\Twig;

class WeatherExtension extends \Twig_Extension {
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('weatherIcon', array($this, 'weatherIconFilter')),
        );
    }

    public function weatherIconFilter($content,$weatherType, $size = "small") {
        $html = "";

        switch ($weatherType){
            case "fog":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"M\"></span>";
                break;
            case "tstorms":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"O\"></span>";
                break;
            case "partlycloudy":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"N\"></span>";
                break;
            case "mostlycloudy":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size\" data-icon=\"N\"></span>";
                break;
            case "cloudy":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"N\"></span>";
                break;
            case "mostlycloudy":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"N\"></span>";
                break;
            case "chancetstorms":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"O\"></span>";
                break;
            case "chancerain":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"Q\"></span>";
                break;
            case "clear":
                $html = "<span href=\"\" class=\"weather-icon weather-icon-block weather-icon-$size \" data-icon=\"A\"></span>";
                break;

        }
        if($html ==""){
            $html = $weatherType;
        }

        return $html;
    }

    public function getName() {
        return 'app_extension';
    }
}

?>