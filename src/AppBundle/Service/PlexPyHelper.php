<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class PlexPyHelper {

    public function __construct(Container $container) {
        $this->container = $container;
//        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function getRecentlyAdded($count) {
        $plexpy_api_key = $this->container->getParameter('settings.plexpyapikey');
        $plexpy_location = $this->container->getParameter('settings.plexpylocation');

        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $plexpy_location."/api/v2?apikey=".$plexpy_api_key."&cmd=get_recently_added&count=".$count."&out_type=json");
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $output = json_decode(curl_exec($ch),true);
        // close curl resource to free up system resources
        curl_close($ch);

        if($output["response"]["result"] == "success"){
            return $output["response"]["data"]["recently_added"];
        }else{
            return false;
        }

    }
}