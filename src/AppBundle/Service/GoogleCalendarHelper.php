<?php

namespace AppBundle\Service;

use Google_Client;
use Google_Service_Calendar;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class GoogleCalendarHelper {

    public function __construct(Container $container) {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->rootpath = realpath($this->container->getParameter('kernel.root_dir').'/..');
    }

    public function getEvents($maxResults) {
        $events = array();
        $client = $this->getClient("personalHub", $this->rootpath."/web/credentials/calendar-php-quickstart.json", $this->rootpath.'/web/credentials/client_secret.json');
        $service = new Google_Service_Calendar($client);

        // Print the next 10 events on the user's calendar.
        $calendarId = 'primary';
        $optParams = array(
            'maxResults' => $maxResults,
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);

        if(count($results->getItems()) == 0) {
            return "No upcoming events found.\n";
        } else{

            foreach ($results->getItems() as $key => $event) {
//                $startString = implode(",", $event["modelData"]["start"]);
//                echo "<pre>";
//                \Doctrine\Common\Util\Debug::dump($startString);
//                echo "</pre>";
//                die();

                $start = new \DateTime(implode(",", $event["modelData"]["start"]));
                $end = new \DateTime(implode(",", $event["modelData"]["end"]));
                $date = new \DateTime(implode(",", $event["modelData"]["start"]));

                //Check if multi day event
                if($date->format("d-m-Y") != $end->format("d-m-Y") && $start->format("H:i:s") !="00:00:00") {
                    //Multi day event
                    $events[$key]["date"] = $date->format("d-m-Y")." - ".$end->format("d-m-Y");
                } else{
                    //Single day event
                    $events[$key]["date"] = $date->format("d-m-Y");
                }
                $events[$key]["wholeDay"] = false;
                //If start and end equals 00:00:00, it a whole day event
                if($start->format("H:i:s") =="00:00:00" && $end->format("H:i:s") =="00:00:00"  ){
                    $events[$key]["wholeDay"] = true;
                }

                $events[$key]["start"] = $start->format("H:i");
                $events[$key]["end"] = $end->format("H:i");
                $events[$key]["summary"] = $event["summary"];
            }

            return $events;
        }
    }


    public function getClient($applicationName, $credentialsPathFile, $clientSecretPath) {

        $scopes = implode(' ', array(Google_Service_Calendar::CALENDAR_READONLY));
        $client = new Google_Client();
        $client->setApplicationName($applicationName);
        $client->setScopes($scopes);
        $client->setAuthConfigFile($clientSecretPath);
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->expandHomeDirectory($credentialsPathFile);
        if(file_exists($credentialsPath)) {
            $accessToken = file_get_contents($credentialsPath);
        } else{
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->authenticate($authCode);

            // Store the credentials to disk.
            if(!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, $accessToken);
//            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if($client->isAccessTokenExpired()) {
            $client->refreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, $client->getAccessToken());
        }
        return $client;
    }


    /**
     * Expands the home directory alias '~' to the full path.
     * @param string $path the path to expand.
     * @return string the expanded path.
     */
    private function expandHomeDirectory($path) {
        $homeDirectory = getenv('HOME');
        if(empty($homeDirectory)) {
            $homeDirectory = getenv("HOMEDRIVE").getenv("HOMEPATH");
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }


}