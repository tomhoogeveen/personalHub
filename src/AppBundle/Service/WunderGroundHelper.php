<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Vaartland\BroadsoftBundle\Entity\BroadsoftCall;

class WunderGroundHelper {

    public function __construct(Container $container) {
        $this->container = $container;
//        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function getWeather($apiKey, $lat, $long) {
        $json_string = file_get_contents("http://api.wunderground.com/api/".$apiKey."/forecast/q/".$lat.",".$long.".json");
        $parsed_json = json_decode($json_string,true);
        $forecast = array();

        foreach ($parsed_json["forecast"]["simpleforecast"]["forecastday"] as $key => $day) {

            $forecast[$key]["date"] = $day["date"]["day"].'-'.$day["date"]["month"].'-'.$day["date"]["year"];
            $forecast[$key]["icon"] = $day["icon"];
            $forecast[$key]["conditions"] = $day["conditions"];
            $forecast[$key]["max"] = $day["high"]["celsius"];
            $forecast[$key]["min"] = $day["low"]["celsius"];
        }
        return $forecast;

//        return $parsed_json["forecast"]["simpleforecast"]["forecastday"];

//        echo "Current temperature in ${location} is: ${temp_f}\n";

    }
}

?>


