<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class PushBulletHelper {

    public function __construct(Container $container) {
        $this->container = $container;
//        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function getChannelInfo($channelName) {
        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, "https://api.pushbullet.com/v2/channel-info?tag=".$channelName."&no_recent_pushes=false");
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        return json_decode($output,true);

    }


}