<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Vaartland\BroadsoftBundle\Entity\BroadsoftCall;

class GreeterHelper {

    public function __construct(Container $container) {
        $this->container = $container;
//        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function getGreeting() {
        $greet = "";
        $date = new \DateTime("now");
        $time = $date->format("H");
        if($time < 12 && $time > 6){
            $greet = "Good Morning";
        }elseif($time > 12 && $time < 18){
            $greet = "Good Afternoon";
        }elseif($time > 18 ){
            $greet = "Good Night";
        }
        return $greet;
    }


}
?>