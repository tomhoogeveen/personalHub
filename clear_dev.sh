#!/usr/bin/env bash
php bin/console cache:clear --env=dev --no-debug
sudo chmod -R 0777 app/logs/ app/cache/ app/spool/
chown -R www-data:www-data /var/www/html/
php bin/console assets:install web
chown -R www-data:www-data /var/www/html/
php bin/console assetic:dump --env=dev --no-debug
chown -R www-data:www-data /var/www/html/
php bin/console doctrine:schema:update --force
chown -R www-data:www-data /var/www/html/
